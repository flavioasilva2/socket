#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from socket import (socket, AF_INET, SOCK_STREAM)
from threading import Thread
from threading import Lock

clientList = []
clientNumber = 0
clientNumberLock = Lock()

class Client(Thread):
    __address = None
    __sock = None
    __clientNumber = None
    __incomingMessages = []
    __outcomingMessages = []

    def __init__(self, clientSock, clientAddress):
        Thread.__init__(self)
        global clientNumber
        self.__sock = clientSock
        self.__address = clientAddress
        clientNumberLock.acquire()
        self.__clientNumber = clientNumber
        clientNumber += 1
        clientNumberLock.release()

    def pushMessageToClient(self, msg):
        self.__outcomingMessages.append(msg)

    def getMesagesFromClient(self):
        result = self.__incomingMessages
        self.__incomingMessages = [] # Lock? 
        return result

    def run(self):
        pass

def bindLoop(sock):
    while True:
        (conn, addr) = sock.accept()
        tmpClient = Client(conn, addr)
        tmpClient.start()
        clientList.append(tmpClient)
    
def main(args):
    PORT = 7777
    IP = '0.0.0.0'
    for i in args:
        arg = i.split('=')
        if arg[0] == '--port':
            PORT = int(arg[1])
        if arg[0] == '--ip':
            IP = arg[1]
    print("Using ip {} and port {}. For diferent values use --port=<port> and --ip=<ip>".format(IP, PORT))
    sockAddress = (IP, PORT)
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind(sockAddress)
    sock.listen(1)
    print("Server listening on {}, port {}".format(IP, PORT))
    bindLoopThread = Thread(target=bindLoop, args=(sock,))
    bindLoopThread.start()
    while True: # msg loop
        for cli in clientList:
            msgs = get

if __name__ == '__main__':
    sys.exit(main(sys.argv))
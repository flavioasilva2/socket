#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from socket import (socket, AF_INET, SOCK_STREAM)
from threading import Thread

def reciveMsgs(sock, bufferSize):
    while True:
        msg = sock.recv(bufferSize)
        print("\n" + msg + "\n")

def main(args):
    PORT = 7777
    IP = None
    for i in args:
        arg = i.split('=')
        if arg[0] == '--port':
            PORT = int(arg[1])
        if arg[0] == '--ip':
            IP = arg[1]
    if IP == None:
        print("Please inform server ip address.")
        return 1
    print("Connecting to server on address {} and port {}. For diferent values use --port=<port> and --ip=<ip>".format(IP, PORT))
    print("Type \q to quit.")
    sockAddress = (IP, PORT)
    sock = socket(AF_INET, SOCK_STREAM)
    try:
        sock.connect(sockAddress)
    except ConnectionRefusedError as e:
        print("Connection refused. Is server listening on informed address?")
        sock.close()
        return 1
    recvThread = Thread(target=reciveMsgs, args=(sock,1024))
    recvThread.start()
    while True:
        msgToSend = input()
        if msgToSend == '\q':
            msgToSend = msgToSend.encode('ascii')
            sock.send(msgToSend)
            print("Bye bye.")
            break
        msgToSend = msgToSend.encode('ascii')
        sock.send(msgToSend)
    sock.close()
    return 0
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))
